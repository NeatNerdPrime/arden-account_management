type Account_management::User = Hash[
  String,
  Variant[
    String,
    Boolean,
    Integer,
    Array[String],
  ],
]
