# frozen_string_literal: true

require 'spec_helper_acceptance'

describe 'account_management' do
  context 'standard config with one user' do
    let(:manifest) do
      <<-EOS
        class { 'account_management':
          required_groups => {
            'bridge' => {
              'name' => 'bridge',
              'gid'  => '1701',
            },
          },
          required_users => {
            'wriker' => {
              'comment' => 'beard',
              'groups'  => [
                'wheel',
                'bridge',
              ],
              'uid'     => '1200',
              'gid'     => '1200',
              'shell'   => '/bin/zsh',
            },
          },
          exec_map       => {
            'wriker' => [
              {
                'command' => 'touch ./baz',
                'cwd'     => '/home/wriker',
                'path'    => '/usr/local/bin:/usr/bin',
                'creates' => '/home/wriker/baz',
              },
            ],
          },
        }
      EOS
    end

    it 'applies without errors and is idempotent on the second run' do
      apply_manifest(manifest, catch_failures: true, debug: false, trace: true)
      apply_manifest(manifest, catch_changes: true, debug: false, trace: true)
    end

    describe package('zsh') do
      it { is_expected.to be_installed }
    end

    describe group('bridge') do
      it 'creates bridge as expected' do
        is_expected.to exist
        is_expected.to have_gid 1701
      end
    end

    describe group('wriker') do
      it 'creates wriker\'s group' do
        is_expected.to exist
        is_expected.to have_gid 1200
      end
    end

    describe user('wriker') do
      it 'creates wriker as specified' do
        is_expected.to exist
        is_expected.to have_uid 1200
        is_expected.to belong_to_primary_group 'wriker'
        is_expected.to have_login_shell '/bin/zsh'
        is_expected.to have_home_directory '/home/wriker'
        is_expected.to belong_to_group 'wheel'
        is_expected.to belong_to_group 'bridge'
      end
    end
  end
end
