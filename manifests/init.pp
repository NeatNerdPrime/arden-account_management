# A class to ensure group and user creation completes 
# without errors.  It ensures a users required groups are 
# created and preffered shell is installed before the 
# user is created.
#
# @summary Controls ordering of group and user creation.
#
# @param required_users
#   A hash of the users to be created with a nested hash 
#   containing all their parameters. See the accounts::user type from
#   puppetlabs/accounts for detail.
#
# @param shell_package_map
#   A hash which maps a given shell absolute path, e.g. /bin/ksh, to the
#   corresponding package. Hiera defaults exist for Gentoo and RedHat family
#   operating systems.
#
# @param required_groups
#   A hash of the groups to be created with a nested hash
#   containing all their parameters.
#
# @param exec_map
#   A hash linking a user to a list of exec command arguments which should be
#   run once upon the creation of the user. 
#
# @param cron_map
#   A hash linking a user to a list of crontab entries which should be
#   scheduled.
#
# @example
#   include account_management
class account_management (
  Hash[String, Account_management::User] $required_users,
  Hash[String, Account_management::Group] $required_groups,
  Account_management::ShellMap $shell_package_map,
  Hash[String, Array[Account_management::ExecOnce]] $exec_map = {},
  Hash[String, Array[Account_management::UserCron]] $cron_map = {},
) {
  contain account_management::groups
  contain account_management::users
  contain account_management::shells

  # Make sure everything happens in the correct order.
  Class['account_management::groups']
  -> Class['account_management::shells']
  -> Class['account_management::users']
}
