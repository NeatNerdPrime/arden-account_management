# A class to install a users preffered shell before the
# user is created.
#
# @summary Installs shells before creating users
#
class account_management::shells {
  $required_users = $account_management::required_users
  $shell_package_map = $account_management::shell_package_map

  # Filters out any users that dont have a shell attribute
  $shell_users = $required_users.filter |$user| {
    'shell' in $user[1]
  }

  # Creates an array of all preferred shells
  $required_shells = $shell_users.map |$entry, $value| {
    if $value['shell'] in $shell_package_map {
      $value['shell']
    } else {
      $shell_path = $value['shell']
      fail("account_management: Un-mapped shell '${shell_path}' specified for user '${entry}'!")
    }
  }

  # Strip out any duplicate entries
  $unique_shells = $required_shells.unique

  # Loop through each unique shell entry and install it
  $unique_shells.each |$shell_path| {
    if $shell_path in $shell_package_map {
      package { $shell_package_map[$shell_path]:
        ensure => present,
      }
    }
  }
}
