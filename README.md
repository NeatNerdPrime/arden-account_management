
# account_management

[![Build Status](https://gitlab.com/arden-puppet/arden-account_management/badges/master/pipeline.svg)](https://gitlab.com/arden-puppet/arden-account_management/commits/master) [![Puppet Forge](https://img.shields.io/puppetforge/v/arden/account_management.svg)](https://forge.puppetlabs.com/arden/account_management)

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with account_management](#setup)
    * [What account_management affects](#what-account_management-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with account_management](#beginning-with-account_management)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

Creates users/groups and installs a users preferred shell in the correct order.

## Setup

### What account_management affects 

It creates users/groups and installs a users preferred shell. It makes sure that groups are created and preferred shells are installed before the user is created.

## Usage

The example below will create a group named bridge and a user named wriker. The bridge group will be created first, then wriker's preferred shell will be installed and finally the wriker user will be created with the requested parameters.

```yaml

profile::account_management::required_groups:
  bridge:
    name: 'bridge'
    gid: '1701'
profile::account_management::required_users:
  wriker:
    comment: 'beard'
    groups:
      - 'wheel'
      - 'bridge'
    uid: '1200'
    gid: '1200'
    shell: '/bin/zsh'
```

### Cron & Exec-Once

This module also supports providing a list of cron entries and run-once executables specified on a per user basis. This can be useful if some kind of initialization is desired when a user is first created. For example, cloning a set of dotfiles from a git repository.

```yaml
profile::account_management::exec_map:
  wriker:
    - command: 'chezmoi init --apply https://github.com/wriker/dotfiles.git'
      path: '/usr/local/bin:/usr/bin'
      environment: [ 'HOME=/home/wriker' ]
      creates: '/home/wriker/.local/share/chezmoi'
profile::account_management::cron_map:
  wriker:
    - command: '/usr/local/bin/chezmoi update -a'
      hour: '0'
      minute: '30'
```

Note that each entry in a user array provided within exec_map *must* include one of the following three attributes:
* `refreshonly`
* `creates`
* `onlyif`

In the example above wriker's [chezmoi](https://github.com/twpayne/chezmoi) configuration is installed when his user is first created. Additionally, a cron entry to refresh that configuration is scheduled to run every day at 00:30.

## Limitations

### OS Support

Tested on:
* CentOS 7
* RHEL 7
* Gentoo
* Debian 10

### Shell Support

* Currently only supports bash, zsh, and ksh.

## Development

See the [contributing guide](https://gitlab.com/arden-puppet/arden-account_management/blob/master/CONTRIBUTING.md).

## Release Notes/Contributors/Etc.

Check out the [contributor list](https://gitlab.com/arden-puppet/arden-account_management/graphs/master)
